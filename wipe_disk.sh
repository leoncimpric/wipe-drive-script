#!/bin/bash
echo $(date) > /root/wipe.log
echo 'start zeroes' >> /root/wipe.log
time dd if=/dev/zero of=/dev/sda bs=10M status=progress

echo $(date) >> /root/wipe.log
echo 'start random' >> /root/wipe.log
time dd if=/dev/urandom of=/dev/sda status=progress

echo $(date) >> /root/wipe.log
echo 'start zeroes' >> /root/wipe.log
time dd if=/dev/zero of=/dev/sda bs=10M status=progress

echo $(date) >> /root/wipe.log
echo 'finished' >> /root/wipe.log
